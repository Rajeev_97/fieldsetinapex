import { LightningElement,wire,track} from 'lwc';
import getFieldSetController from '@salesforce/apex/FieldSetController.getFieldSetController';
export default class FieldSetController extends LightningElement {
    @track columns ;
    @track error ;
    @track data ;
    get columns[
        { label: 'Name', fieldName: 'Name', type: 'text' },
        { label: 'Billing state', fieldName: 'BillingState', type: 'text' },
    ]
    @wire(getFieldSetController)
    wiredAccounts({
        error,
        data
    }) {
        if (data) {
            this.data = data;
            //console.log(JSON.stringify(data, null, '\t'));
        } else if (error) {
            this.error = error;
        }
    }
}